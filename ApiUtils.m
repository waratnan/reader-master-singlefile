//
//  ApiUtils.m
//  EMagazine
//
//  Created by Waratnan Suriyasorn on 4/2/2557 BE.
//  Copyright (c) 2557 Waratnan Suriyasorn. All rights reserved.
//

#import "ApiUtils.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIProgressView+AFNetworking.h>

//#import "AFHTTPRequestOperation.h"
//#import "AFURLSessionManager.h"

@implementation ApiUtils

-(NSURL*)getDocumentDirectoryPath
{
    NSURL *documentsDirectoryPath = [NSURL fileURLWithPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]];
    
    return documentsDirectoryPath;
    
}

-(void)downloadFile:(NSString*)url_string withName:(NSString*)name progressView:(UIProgressView*)progress
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:url_string];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
//        return [documentsDirectoryPath URLByAppendingPathComponent:[response suggestedFilename]];
        
        [self checkIfDirectoryAlreadyExists:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] dirName:@"PDF_Download"];
        
        return [[self getDocumentDirectoryPath] URLByAppendingPathComponent:[NSString stringWithFormat:@"PDF_Download/%@",name]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
    }];
    
    [progress setProgressWithDownloadProgressOfTask:downloadTask animated:YES];
    
    [manager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        NSLog(@"Progress… %lld", totalBytesWritten);
    }];
    
    [downloadTask resume];
    
}


-(void)openBrowser:(NSString *)url_string
{
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:url_string]])
        NSLog(@"%@%@",@"Failed to open url:",[url_string description]);
}

-(void)checkIfDirectoryAlreadyExists:(NSString *)path dirName:(NSString*)name
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    BOOL isDir;
    BOOL exists = [fileManager fileExistsAtPath:[NSString stringWithFormat:@"%@/%@",path,name] isDirectory:&isDir];
    
    if (!exists) {
        /* file exists */
        [self createDirectory:name atFilePath:path];
    }
    
    if (isDir) {
        /* file is a directory */
        NSLog(@"exists");
    }
}

-(void)createDirectory:(NSString *)directoryName atFilePath:(NSString *)filePath
{
    NSString *filePathAndDirectory = [filePath stringByAppendingPathComponent:directoryName];
    NSError *error;
    
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePathAndDirectory
                                   withIntermediateDirectories:NO
                                                    attributes:nil
                                                         error:&error])
    {
        NSLog(@"Create directory error: %@", error);
    }
    
}

-(NSArray*)fetchAllPDFInDir
{
    
    NSURL *DirURL = [[self getDocumentDirectoryPath] URLByAppendingPathComponent:@"PDF_Download"];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray * dirContents = [fm contentsOfDirectoryAtURL:DirURL
      includingPropertiesForKeys:@[]
                         options:NSDirectoryEnumerationSkipsHiddenFiles
                           error:nil];
    NSPredicate * fltr = [NSPredicate predicateWithFormat:@"pathExtension='pdf'"];
    NSArray * onlyPDF = [dirContents filteredArrayUsingPredicate:fltr];
//    NSLog(@"%@",onlyPDF);
    return  onlyPDF;
    
}

-(void)downloadFile:(NSString *)UrlAddress name:(NSString*)name progressView:(UIProgressView*)progress
{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:UrlAddress]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:name];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Successfully downloaded file to %@", path);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, NSInteger totalBytesRead, NSInteger totalBytesExpectedToRead) {
        
//      NSLog(@"%@ Download = %f ",name, (float)totalBytesRead / totalBytesExpectedToRead);
//      float progress =(float)totalBytesRead / totalBytesExpectedToRead;
        NSMutableDictionary* userInfo = [NSMutableDictionary dictionary];
        [userInfo setObject:name forKey:@"fileName"];
        [userInfo setObject:[NSNumber numberWithInteger:totalBytesRead] forKey:@"totalBytesRead"];
        [userInfo setObject:[NSNumber numberWithInteger:totalBytesExpectedToRead] forKey:@"totalBytesExpectedToRead"];
//        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:totalBytesRead forKey:@"progress"];
        [[NSNotificationCenter defaultCenter] postNotificationName: @"TestNotification" object:nil userInfo:userInfo];
    }];

    [progress setProgressWithDownloadProgressOfOperation:operation animated:YES];
//    [operation start];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
    

}

@end