//
//  ApiUtils.h
//  EMagazine
//
//  Created by Waratnan Suriyasorn on 4/2/2557 BE.
//  Copyright (c) 2557 Waratnan Suriyasorn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiUtils : NSObject


-(void)downloadFile:(NSString*)url_string withName:(NSString*)name progressView:(UIProgressView*)progress;
-(void)openBrowser:(NSString*)url_string;
-(NSArray*)fetchAllPDFInDir;
-(void)downloadFile:(NSString *)UrlAddress name:(NSString*)name index:(int)index progressView:(UIProgressView*)progress;

@end
